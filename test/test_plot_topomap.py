import mne
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from helpers.plot import pos_test, names_test


pat_p = np.loadtxt('./data/test_plot_topomap/pat_p')
assert(pat_p.shape[0] == pos_test.shape[0])

pat_q = np.loadtxt('./data/test_plot_topomap/pat_q')
assert(pat_q.shape[0] == pos_test.shape[0])

#print(matplotlib.colormaps)
fig, axs = plt.subplots(1, 2)
axs[0].set_title('pat_p')
mne.viz.plot_topomap(pat_p, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[0], names=names_test, show=False)
axs[1].set_title('pat_q')
mne.viz.plot_topomap(pat_q, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[1], names=names_test, show=False)
plt.show()
