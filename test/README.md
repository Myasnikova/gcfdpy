# gcfdpy test

Install virtual env

```
sudo apt install python3.10-venv
```

Install tkinter

```
sudo apt install python3-tk
```

Create virtual env:

```
python3 -m venv .venv
```

Avtivate virtual env:

```
source .venv/bin/activate
```

Inastall dependencies:

```
pip install -r requirements.txt
```

Test `plot_topomap`:

```
python test_plot_topomap.py
```

Reference plot from MATLAB:

![plot](./data/test_plot_topomap/ref.png)

Test `gcfd`:

```
python test_gcfd.py
```

Reference plot from MATLAB:

![plot](./data/test_gcfd/ref.png)

Test `gcfd PairsNum: 2`:

```
python test_gcfd_PairsNum_2.py
```

Reference plot from MATLAB:

![plot](./data/test_gcfd_PairsNum_2/ref.png)

Test `gcfd PairsNum: 3`:

```
python test_gcfd_PairsNum_3.py
```

Reference plot from MATLAB:

![plot](./data/test_gcfd_PairsNum_3/ref.png)

