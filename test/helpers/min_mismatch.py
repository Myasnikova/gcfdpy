import numpy as np
from helpers.mat_mismatch import mat_mismatch


def min_mismatch(A, B, sub_size):
    assert A.shape == B.shape
    assert sub_size < A.shape[0]
    col = A.shape[1]
    for i in range(col):
        min_mismatch = mat_mismatch(A[:sub_size,i], B[:sub_size,i]) + mat_mismatch(A[sub_size:,i], B[sub_size:,i])
        for j in range(i+1, col):
            cur_mismatch = mat_mismatch(A[:sub_size,i], B[:sub_size,j]) + mat_mismatch(A[sub_size:,i], B[sub_size:,j])
            if (cur_mismatch < min_mismatch):
                tmp = np.array(B[:,i])
                B[:,i] = B[:,j]
                B[:,j] = tmp
                min_mismatch = cur_mismatch
    return B

