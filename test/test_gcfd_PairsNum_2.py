from nsflow.gcfd import gcfd
from nsflow.kuramoto import kuramoto
import mne
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from helpers.plot import pos_test, names_test
from helpers.mat_mismatch import mat_mismatch


pat_p_src = np.loadtxt('./data/test_gcfd_PairsNum_2/pat_p', delimiter = ',')
pat_q_src = np.loadtxt('./data/test_gcfd_PairsNum_2/pat_q', delimiter = ',')

PairsNum = 2
FrBase = 10
FrWidth = 1.0
FrP = 1
FrQ = 2
pat_p, pat_q, sig_p, sig_q = gcfd(np.loadtxt("./data/test_gcfd_PairsNum_2/elec.txt", delimiter = ','),
        PairsNum=PairsNum, FrBase=FrBase, FrWidth=FrWidth, FrP=FrP, FrQ=FrQ)

fig, axs = plt.subplots(4, 2)

pat_p_src1 = pat_p_src[:,0].reshape(pat_p_src[:,0].shape[0],)
axs[0, 0].set_title('PAT ' + str(FrBase*FrP) + 'HZ')
mne.viz.plot_topomap(pat_p_src1, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[0, 0], show=False)

pat_q_src1 = pat_q_src[:,0].reshape(pat_q_src[:,0].shape[0],)
axs[1, 0].set_title('PAT ' + str(FrBase*FrQ) + 'HZ')
mne.viz.plot_topomap(pat_q_src1, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[1, 0], show=False)

pat_p1 = pat_p[:,0].reshape(pat_p[:,0].shape[0],)
err_p1 = mat_mismatch(pat_p1, pat_p_src1)
assert(err_p1 < 0.02)
axs[2, 0].set_title('CFD (err: ' + str(round(err_p1, 2)) + ')')
mne.viz.plot_topomap(pat_p1, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[2, 0], show=False)

pat_q1 = pat_q[:,0].reshape(pat_q[:,0].shape[0],)
err_q1 = mat_mismatch(pat_q1, pat_q_src1)
assert(err_q1 < 0.07)
sig_p1 = sig_p[:,0].reshape(sig_p[:,0].shape[0],)
sig_q1 = sig_q[:,0].reshape(sig_q[:,0].shape[0],)
k1 = kuramoto(sig_p1, sig_q1, FrP, FrQ)
assert(k1 > 0.34)
axs[3, 0].set_title('CFD (err: ' + str(round(err_q1, 2)) + ', k: ' + str(round(k1, 2)) + ')')
mne.viz.plot_topomap(pat_q1, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[3, 0], show=False)

pat_p_src2 = pat_p_src[:,1].reshape(pat_p_src[:,1].shape[0],)
axs[0, 1].set_title('PAT ' + str(FrBase*FrP) + 'HZ')
mne.viz.plot_topomap(pat_p_src2, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[0, 1], show=False)

pat_q_src2 = pat_q_src[:,1].reshape(pat_q_src[:,1].shape[0],)
axs[1, 1].set_title('PAT ' + str(FrBase*FrQ) + 'HZ')
mne.viz.plot_topomap(pat_q_src2, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[1, 1], show=False)

pat_p2 = pat_p[:,1].reshape(pat_p[:,1].shape[0],)
err_p2 = mat_mismatch(pat_p2, pat_p_src2)
assert(err_p2 < 0.09)
axs[2, 1].set_title('CFD (err: ' + str(round(err_p2, 2)) + ')')
mne.viz.plot_topomap(pat_p2, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[2, 1], show=False)

pat_q2 = pat_q[:,1].reshape(pat_q[:,1].shape[0],)
err_q2 = mat_mismatch(pat_q2, pat_q_src2)
assert(err_q2 < 0.05)
sig_p2 = sig_p[:,1].reshape(sig_p[:,1].shape[0],)
sig_q2 = sig_q[:,1].reshape(sig_q[:,1].shape[0],)
k2 = kuramoto(sig_p2, sig_q2, FrP, FrQ)
#assert(k2 > 0.58)
axs[3, 1].set_title('CFD (err: ' + str(round(err_q2, 2)) + ', k: ' + str(round(k2, 2)) + ')')
mne.viz.plot_topomap(pat_q2, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[3, 1], show=False)

plt.show()
