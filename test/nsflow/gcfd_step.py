from nsflow.fun_gcfd import fun_gcfd
import numpy as np
from scipy.signal import hilbert
from scipy.optimize import least_squares
from scipy.signal import detrend


def gcfd_step(f1, f2, m, n, f0, comp_num, pat_ssd, PhaseShift):

    f1 = hilbert(f1)
    r1 = np.abs(f1) * np.exp(1j * (m * np.angle(f1) + PhaseShift))

    M2 = f2

    # algo
    res_filter = least_squares(fun_gcfd, f0, args=(M2, n, r1, PhaseShift), ftol=1e-14, method='lm', max_nfev=10000)
    #print(res_filter)
    filter_x = res_filter.x

    sig = np.dot(M2, filter_x);

    # filter to pattern
    V = np.dot(M2,filter_x);
    V = detrend(V, axis=0, type="constant")
    pattern = np.dot(M2.T, V) / np.sqrt(np.dot(V.T, V))

    if comp_num:
        # Convert the pattern in SSD coordinates to the pattern in original coordinates
        pat = np.zeros(pat_ssd.shape[0]);
        pat[:comp_num] = pattern;
        pattern = np.dot(pat_ssd, pat);

    return pattern, sig

