import numpy as np
from scipy import signal
from scipy.signal import butter
from numpy import linalg as LA
from scipy import linalg as LA2

def ssd(concat_elec, FrX, FrBase, FrWidth, Discr):
    T = np.shape(concat_elec)[0]
    filter_order = 2
    nyq = float(Discr) / 2

    #creating central frequency
    signal_band1 = [FrX*FrBase-1*FrWidth,FrX*FrBase+1*FrWidth]
    signal_band = [float(signal_band1[0]) / nyq, float(signal_band1[1]) / nyq]
    b, a = butter(filter_order, signal_band, 'bandpass')

    #fast but not accurate version
    #TODO: add options
    '''
    concat_elec = np.reshape(concat_elec, (T*14,1), order='F')
    print "concat_elec[0:20]"
    print concat_elec[0:20]
    X_s = signal.filtfilt(b,a, np.ravel(concat_elec, order='F'), padtype = 'odd', padlen=3*(max(len(b),len(a))-1));
    X_s = np.reshape(X_s, (T,14), order='F')
    #X_s = np.around(X_s,4)
    np.savetxt("X_s.txt", X_s[:,0], fmt='%.4f')
    '''

    Len = np.shape(concat_elec)[1]
    X_s = np.zeros((T,Len))
    for n in range(Len):
        X_s[:,n] = signal.filtfilt(b,a, concat_elec[:,n], padtype = 'odd', padlen=3*(max(len(b),len(a))-1));

    C_s = np.cov(X_s.T);

    #filtering flanking frequencies
    noise_bp_band1 = [FrX*FrBase-3*FrWidth,FrX*FrBase+3*FrWidth]
    noise_bp_band = [float(noise_bp_band1[0]) / nyq, float(noise_bp_band1[1]) / nyq]
    noise_bs_band1 = [FrX*FrBase-2*FrWidth,FrX*FrBase+2*FrWidth]
    noise_bs_band = [float(noise_bs_band1[0]) / nyq, float(noise_bs_band1[1]) / nyq]
    b_f, a_f = butter(filter_order,noise_bp_band, 'bandpass')
    b_s, a_s = butter(filter_order,noise_bs_band, 'bandstop')

    X_tmp = np.zeros((T,Len))
    for n in range(Len):
        X_tmp[:,n] = signal.filtfilt(b_f, a_f, concat_elec[:,n], padtype = 'odd', padlen=3*(max(len(b),len(a))-1));

    for n in range(Len):
        X_tmp[:,n] = signal.filtfilt(b_s, a_s, X_tmp[:,n], padtype = 'odd', padlen=3*(max(len(b),len(a))-1));

    C_n = np.cov(X_tmp.T);

    # Generalized eigenvalue decomposition

    # dim-reduction of X does not have full rank
    W, V = LA.eig(C_s);
    idx = np.argsort(W)[::-1]
    W = W[idx]
    V = V[:,idx]
    tol = W[0]*(10**-6)
    r = np.sum(W > tol)
    s = np.shape(concat_elec)[1]
    if r < s:
        print("SSD: Input data does not have full rank. Components:", r)
        M = V[:,1:r] * np.diag(np.sqrt(W[1:r]));
    else:
        M = np.eye(s);

    C_s_r = np.dot(np.dot(M.T, C_s), M)
    C_n_r = np.dot(np.dot(M.T, C_n), M)

    V, W = LA2.eig(C_s_r,C_s_r+C_n_r);
    idx = np.argsort(V)[::-1]
    V = V[idx]
    W = W[:,idx]
    W = np.dot(M,W)

    A = np.dot(np.dot(C_s,W),LA.inv(np.dot(np.dot(W.T, C_s), W)))

    X_ssd = np.dot(X_s, W)

    return W, A, X_ssd

