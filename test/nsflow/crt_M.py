import numpy as np
from scipy import signal
from scipy.signal import butter
from scipy.signal import detrend

def crt_M(comp_num, FrX, FrBase, FrWidth, Discr, X):
    filter_order = 2
    nyq = float(Discr) / 2

    #creating central frequency
    signal_band1 = [FrX*FrBase-1*FrWidth,FrX*FrBase+1*FrWidth]
    signal_band = [float(signal_band1[0]) / nyq, float(signal_band1[1]) / nyq]
    b, a = butter(filter_order, signal_band, 'bandpass')

    T = np.shape(X)[0]
    M = np.zeros((T,comp_num))
    for n in range(comp_num):
        M[:,n] = signal.filtfilt(b, a, X[:,n], padtype = 'odd', padlen=3*(max(len(b),len(a))-1));

    M = detrend(M, axis=0, type="constant")

    return M

