from scipy.signal import hilbert
import numpy as np


def kuramoto(s1, s2, f1, f2):
    c1 = hilbert(s1)
    c2 = hilbert(s2)
    phase_rel = f2*np.unwrap(np.angle(c1)) - f1*np.unwrap(np.angle(c2))
    k = abs(np.mean(np.exp(1j*phase_rel)))
    return k
