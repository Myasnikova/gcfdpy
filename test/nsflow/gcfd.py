from nsflow.ssd import ssd
from nsflow.crt_M import crt_M
from nsflow.gcfd_step import gcfd_step
from scipy.signal import detrend
import numpy as np


def gcfd(elec, PairsNum=1, FrBase=10, FrWidth=1.0, FrP=1, FrQ=2, Discr=250, comp_num=5, PhaseShift=0):
    W_p, pat_p_ssd, X_ssd_p = ssd(elec, FrP, FrBase, FrWidth, Discr)
    W_q, pat_q_ssd, X_ssd_q = ssd(elec, FrQ, FrBase, FrWidth, Discr)
    M_p = crt_M(comp_num, FrP, FrBase, FrWidth, Discr, X_ssd_p)
    M_q = crt_M(comp_num, FrQ, FrBase, FrWidth, Discr, X_ssd_q)
    pat_p = np.zeros((np.shape(elec)[1],PairsNum))
    pat_q = np.zeros((np.shape(elec)[1],PairsNum))
    sig_p = np.zeros((np.shape(elec)[0],PairsNum))
    sig_q = np.zeros((np.shape(elec)[0],PairsNum))
    for i in range(PairsNum):
        # pat q
        m = FrQ
        n = FrP
        f1 = M_p[:,i]
        f2 = M_q;
        pat_ssd = pat_q_ssd;
        W = W_q[:,i]
        f0 = W[:comp_num]
        pat_q[:,i], sig_q[:,i] = gcfd_step(f1, f2, m, n, f0, comp_num, pat_ssd, PhaseShift)
        # pat p
        m = FrP
        n = FrQ
        f1 = detrend(sig_q[:,i], axis=0, type="constant")
        f2 = M_p;
        pat_ssd = pat_p_ssd;
        W = W_p[:,i]
        f0 = W[:comp_num]
        pat_p[:,i], sig_p[:,i] = gcfd_step(f1, f2, m, n, f0, comp_num, pat_ssd, PhaseShift)
    return pat_p, pat_q, sig_p, sig_q

