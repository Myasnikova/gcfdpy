from nsflow.gcfd import gcfd
from nsflow.kuramoto import kuramoto
import mne
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from helpers.plot import pos_test, names_test
from helpers.mat_mismatch import mat_mismatch
from helpers.min_mismatch import min_mismatch


pat_p_src = np.loadtxt('./data/test_gcfd_PairsNum_3/pat_p', delimiter = ',')
pat_q_src = np.loadtxt('./data/test_gcfd_PairsNum_3/pat_q', delimiter = ',')
assert(pat_p_src.shape == pat_q_src.shape)
pat_size = pat_p_src.shape[0]
pat_num = pat_p_src.shape[1]
pat_src = np.zeros((pat_size * 2, pat_num))
pat_src[:pat_size,:] = pat_p_src
pat_src[pat_size:,:] = pat_q_src

PairsNum = pat_num
FrBase = 10
FrWidth = 1.0
FrP = 1
FrQ = 2
pat_p, pat_q, sig_p, sig_q = gcfd(np.loadtxt("./data/test_gcfd_PairsNum_3/elec.txt", delimiter = ','),
        PairsNum=PairsNum, FrBase=FrBase, FrWidth=FrWidth, FrP=FrP, FrQ=FrQ)
assert(pat_p.shape == pat_q.shape)
assert(pat_size == pat_p.shape[0])
assert(pat_num == pat_p.shape[1])
pat = np.zeros((pat_size * 2, pat_num))
pat[:pat_size,:] = pat_p
pat[pat_size:,:] = pat_q

pat = min_mismatch(pat_src, pat, pat_size)
pat_p = pat[:pat_size,:]
pat_q = pat[pat_size:,:]

fig, axs = plt.subplots(4, PairsNum)
for i in range(PairsNum):
    axs[0, i].set_title('PAT ' + str(FrBase*FrP) + 'HZ')
    mne.viz.plot_topomap(pat_p_src[:,i], pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[0, i], show=False)

    axs[1, i].set_title('PAT ' + str(FrBase*FrQ) + 'HZ')
    mne.viz.plot_topomap(pat_q_src[:,i], pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[1, i], show=False)

    err_p = mat_mismatch(pat_p[:,i], pat_p_src[:,i])
    #assert(err_p < 0.02)
    axs[2, i].set_title('CFD (err: ' + str(round(err_p, 2)) + ')')
    mne.viz.plot_topomap(pat_p[:,i], pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[2, i], show=False)

    err_q = mat_mismatch(pat_q[:,i], pat_q_src[:,i])
    #assert(err_q1 < 0.07)
    k = kuramoto(sig_p[:,i], sig_q[:,i], FrP, FrQ)
    #assert(k1 > 0.34)
    axs[3, i].set_title('CFD (err: ' + str(round(err_q, 2)) + ', k: ' + str(round(k, 2)) + ')')
    mne.viz.plot_topomap(pat_q[:,i], pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[3, i], show=False)
plt.show()

