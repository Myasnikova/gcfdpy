from nsflow.gcfd import gcfd
from nsflow.kuramoto import kuramoto
import mne
import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
from helpers.plot import pos_test, names_test
from helpers.mat_mismatch import mat_mismatch


PairsNum = 1
FrBase = 10
FrWidth = 1.0
FrP = 1
FrQ = 2
pat_p, pat_q, sig_p, sig_q = gcfd(np.loadtxt("./data/test_gcfd/elec.txt", delimiter = ','),
        PairsNum=PairsNum, FrBase=FrBase, FrWidth=FrWidth, FrP=FrP, FrQ=FrQ)
pat_p = pat_p.reshape(pat_p.shape[0],)
pat_q = pat_q.reshape(pat_q.shape[0],)
sig_p = sig_p.reshape(sig_p.shape[0],)
sig_q = sig_q.reshape(sig_q.shape[0],)

fig, axs = plt.subplots(2, 2)

pat_p_src = np.loadtxt('./data/test_plot_topomap/pat_p')
axs[0, 0].set_title('PAT ' + str(FrBase*FrP) + 'HZ')
mne.viz.plot_topomap(pat_p_src, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[0, 0], show=False)

err_p = mat_mismatch(pat_p, pat_p_src)
assert(err_p < 0.03)
axs[1, 0].set_title('CFD (err: ' + str(round(err_p, 2)) + ')')
mne.viz.plot_topomap(pat_p, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[1, 0], show=False)

pat_q_src = np.loadtxt('./data/test_plot_topomap/pat_q')
axs[0, 1].set_title('PAT ' + str(FrBase*FrQ) + 'HZ')
mne.viz.plot_topomap(pat_q_src, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[0, 1], show=False)

err_q = mat_mismatch(pat_q, pat_q_src)
assert(err_q < 0.06)
k = kuramoto(sig_p, sig_q, FrP, FrQ)
assert(k > 0.58)
axs[1, 1].set_title('CFD (err: ' + str(round(err_q, 2)) + ', k: ' + str(round(k, 2)) + ')')
mne.viz.plot_topomap(pat_q, pos_test, sensors=False, cmap='YlGnBu_r', axes = axs[1, 1], show=False)

plt.show()

