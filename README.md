# gcfdpy

In order to migrate from MATLAB cross frequency synchrony analysis ( https://bitbucket.org/Myasnikova/gcfd/src/master/)
we are developing main computational module 'algo' in python.

Algo contains:

extracting the reference signal at frequency p with maximal SNR ratio (ssd)

making an estimate which is used to restore the synchronous signal at frequency q

using non-linear gradient descent and the estimate in order to restore the synch signal

having obtained the signal, compute the topography of the synch sig

https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6200871/

# steps

```
sudo apt install python3.10-venv
```

Create virtual env:

```
python3 -m venv .venv
```

Avtivate virtual env:

```
source .venv/bin/activate
```

Inastall dependencies:

```
pip install -r requirements.txt
```

