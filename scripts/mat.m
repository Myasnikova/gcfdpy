function mat(from,to,name,seed,SSD_file_out)

cd ../../matlab_SPoC
startup_spoc
cd ../gcfd
startup_gcfd
if strcmp(name,'ATANOVMS')
    disp(SSD_file_out)
    algo(front_categ(conf({'debug_py','true','SSD_file_out',SSD_file_out}),from,to,{'permut_seed',seed,'load_concat_elec','true'}))
else
    algo(front_categ(conf({'debug_py','true','SSD_file_out',SSD_file_out}),from,to,{'Name',name,'permut_seed',seed,'load_concat_elec','true'}))
end
exit

