function mat_cmm(from,to,name,seed,SSD_file_out)

cd ../../matlab_SPoC
startup_spoc
cd ../gcfd
startup_gcfd
%algo(front_categ(conf({'debug_py','true','SSD_file_out',SSD_file_out}),from,to,{'permut_seed',seed,'load_concat_elec','true'}))
algo(front_cmm(conf({'motor','false','PairsNum','1','debug_py','true','SSD_file_out',SSD_file_out}),{'stype', 'cmm', 'Name','S','permut_seed','0','load_elec','true','FrBase','9'}))

exit
