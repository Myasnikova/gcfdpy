RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

test_no=1
for currPair in "${Pair[@]}"
do
    currFrom="${From[${currPair}]}"
    currTo="${To[${currPair}]}"
    for currName in "${Name[@]}"
    do
        for currSeed in "${Seed[@]}"
        do
            echo "run test #${test_no} check_mat_py(${currFrom}, ${currTo}, ${currName}, ${currSeed})"
            SSD_out=mat_SSD_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}".out
            mat_SSD_out=${data_dir}/${SSD_out}
            py_SSD_out=${data_dir}/py_SSD_"${currFrom}"_"${currTo}"_"${currName}"_seed_"${currSeed}".out

            if [ "${test_no}" -le 0 ]
            then
                echo -e "${GREEN}Test SKIP${NC}"
                echo ""
                let "test_no=test_no+1"
                continue
            fi

            if [ ! -f "${mat_SSD_out}" ];
            then
            echo "run MATLAB script ..."
            echo "/home/sasha/MATLAB/bin/matlab -nodisplay -nodesktop -logfile mat_cmm_out.log -r \"mat_cmm(${currFrom},${currTo},'${currName}','${currSeed}','${SSD_out}')\""
            /home/sasha/MATLAB/bin/matlab -nodisplay -nodesktop -logfile mat_cmm_out.log -r "mat_cmm(${currFrom},${currTo},'${currName}','${currSeed}','${SSD_out}')" > /dev/null
            echo "DONE"
            fi

            echo "run Python script ..."
            echo "python3 ../src/algo.py --from ${currFrom} --to ${currTo} --name ${currName} --seed ${currSeed}"
            python3 ../src/algo.py --from ${currFrom} --to ${currTo} --name ${currName} --seed ${currSeed}
            echo "DONE"

            echo "run Python check script ..."
#            echo "python ../src/abs_diff.py --fA ${mat_SSD_out} --fB ${py_SSD_out}"
#            res="$(python ../src/abs_diff.py --fA ${mat_SSD_out} --fB ${py_SSD_out})"
            echo "python ../src/mat_mismatch.py --fA ${mat_SSD_out} --fB ${py_SSD_out}"
            res="$(python ../src/mat_mismatch.py --fA ${mat_SSD_out} --fB ${py_SSD_out})"
            echo $res
            TR=0.2
            if (( $(echo "$res > $TR" |bc -l) ))
            then
                echo -e "${RED}Test FAIL${NC}"
                #diff ${mat_SSD_out} ${py_SSD_out}
                exit
            fi
            echo "DONE"

            echo -e "${GREEN}Test PASS${NC}"
            echo ""

            let "test_no=test_no+1"
        done
    done
done
exit


