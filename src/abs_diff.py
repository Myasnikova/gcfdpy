import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--fA')
parser.add_argument('--fB')

args = parser.parse_args()

A = np.loadtxt(args.fA, delimiter = ' ')
B = np.loadtxt(args.fB, delimiter = ' ')

print np.max(np.abs(A-B))

