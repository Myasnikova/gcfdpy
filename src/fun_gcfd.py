import numpy as np

#fun = @(f) abs(exp(1i*PhaseShift)*(M2_adj*f).^n - r1_adj(:));

def fun_gcfd(x, M2, n, r1, PhaseShift):

    x = np.matrix(x)
    x = x.T

    r1 = np.matrix(r1)
    r1 = r1.T

    res = np.abs(np.exp(1j*PhaseShift) * np.power(np.dot(M2,x), n) - r1)
    return np.ravel(res.T)

