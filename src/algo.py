import argparse
import numpy as np
#from scipy import signal
#from scipy.signal import butter
#from scipy.signal import hilbert
#from numpy import linalg as LA
#from scipy import linalg as LA2
#from scipy.optimize import least_squares
from scipy.signal import detrend

from ssd import ssd
from crt_M import crt_M
#from fun_gcfd import fun_gcfd
from gcfd_step import gcfd_step

parser = argparse.ArgumentParser()
parser.add_argument('--from1')
parser.add_argument('--to')
parser.add_argument('--name')
parser.add_argument('--seed')
args = parser.parse_args()

fname = "../../data_cmm_concat_cmm_r/concat_elec_"
fname = fname + args.name + "_type_cmm_seed_" + args.seed + ".elec"

fnameout = "../../data_check_mat_py/py_SSD_" + args.from1 + "_" + args.to
fnameout = fnameout + "_" + args.name + "_seed_" + args.seed + ".out"

concat_elec = np.loadtxt(fname, delimiter = ',')

FrBase = 10
FrWidth = 1.0
FrP = 1
FrQ = 2
Discr = 250

# ssd P
W_p, pat_p_ssd, X_ssd_p = ssd(concat_elec, FrP, FrBase, FrWidth, Discr)

# ssd Q
W_q, pat_q_ssd, X_ssd_q = ssd(concat_elec, FrQ, FrBase, FrWidth, Discr)

comp_num = 5

# M_p
M_p = crt_M(comp_num, FrP, FrBase, FrWidth, Discr, X_ssd_p)

# M_q
M_q = crt_M(comp_num, FrQ, FrBase, FrWidth, Discr, X_ssd_q)

PhaseShift = 0

# algo
PairsNum = 1
for i in range(PairsNum):
    # pat q
    m = FrQ
    n = FrP
    f1 = M_p[:,i]
    f2 = M_q;
    pat_ssd = pat_q_ssd;
    W = W_q[:,i]
    f0 = W[:comp_num]
    pat_q, cur_sig_q = gcfd_step(f1, f2, m, n, f0, comp_num, pat_ssd, PhaseShift)

    # pat p
    m = FrP
    n = FrQ
    cur_sig_q = detrend(cur_sig_q, axis=0, type="constant")
    f1 = cur_sig_q
    f2 = M_p;
    pat_ssd = pat_p_ssd;
    W = W_p[:,i]
    f0 = W[:comp_num]
    pat_p, cur_sig_p = gcfd_step(f1, f2, m, n, f0, comp_num, pat_ssd, PhaseShift)

np.savetxt(fnameout, pat_p, fmt='%.5f')

