import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--fA')
parser.add_argument('--fB')

args = parser.parse_args()

A = np.loadtxt(args.fA, delimiter = ' ')
B = np.loadtxt(args.fB, delimiter = ' ')

max_mismatch = 0;

assert np.shape(A) == np.shape(B)
if A.ndim == 1:
    V1 = A
    V2 = B
    cur_mismatch = 1 - np.abs(np.sum(np.dot(V1,V2)) / (np.sqrt(np.sum(np.dot(V1,V1))) * np.sqrt(np.sum(np.dot(V2,V2)))))
    max_mismatch = cur_mismatch
else:
    col = A.shape[1]
    for i in range(0,col):
        V1 = A[:,i]
        V2 = B[:,i]
        cur_mismatch = 1 - np.abs(np.sum(np.dot(V1,V2)) / (np.sqrt(np.sum(np.dot(V1,V1))) * np.sqrt(np.sum(np.dot(V2,V2)))))
        if cur_mismatch > max_mismatch:
            max_mismatch = cur_mismatch

print("%.2f" % max_mismatch)

