import argparse
import numpy as np
from scipy.optimize import least_squares

from fun_gcfd import fun_gcfd

parser = argparse.ArgumentParser()
parser.add_argument('--fM2')
parser.add_argument('--fr1r')
parser.add_argument('--fr1i')
parser.add_argument('--ff0')

args = parser.parse_args()

M2 = np.loadtxt(args.fM2, delimiter = ' ')
r1r = np.loadtxt(args.fr1r, delimiter = ' ')
r1i = np.loadtxt(args.fr1i, delimiter = ' ')
f0 = np.loadtxt(args.ff0, delimiter = ' ')

n = 1
r1 = r1r + 1j * r1i

f0 = f0 * 0.5633520240700218
r1 = r1 * 1.263287526906479
M2[:,0] = M2[:,0] * (-1.0)

res_filter = least_squares(fun_gcfd, f0, args=(M2, n, r1), ftol=1e-14, xtol=1e-6, method='lm', max_nfev=10000, verbose=2)
print res_filter

